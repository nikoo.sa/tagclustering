import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class Main
{
    public static void main(String args[]) throws IOException {
        String Path = "MyData.txt";
        FileReader fr = new FileReader(Path);
        BufferedReader br = new BufferedReader(fr);
        String line = null;

        Map<String, ArrayList<String>> mymap = new HashMap<>();
        Map<String, ArrayList<String>> tagsmap = new HashMap<>();
        Map<String, Float> tagscount = new HashMap<>();

        ArrayList<String> data = new ArrayList<>();
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<String> keys2 = new ArrayList<>();
        ArrayList<String> newkeys = new ArrayList<>();

        int cnt = 0;
        String x = null;
        while ((line = br.readLine()) != null) {
            data.clear();
            StringTokenizer tokenizer = new StringTokenizer(line);
            while(tokenizer.hasMoreTokens()) {
                data.add(tokenizer.nextToken());
            }
            if(data.size() >= 2) {
                String key = data.get(0);
                String value = data.get(1);
                if(mymap.containsKey(key) && value != "Tags")
                {
                    ArrayList<String> list = mymap.get(key);
                    list.add(value);
                    mymap.remove(key);
                    mymap.put(key, list);
                }
                else if(value != "Tags")
                {
                    ArrayList<String> list = new ArrayList<>();
                    list.add(value);
                    mymap.put(key, list);
                    keys.add(key);
                }
                String key2 = data.get(1);
                String value2 = data.get(0);
                if(tagsmap.containsKey(key2) && value2 != "Id")
                {
                    ArrayList<String> list = tagsmap.get(key2);
                    list.add(value2);
                    tagsmap.remove(key2);
                    tagsmap.put(key2, list);
                }
                else if(value2 != "Id")
                {
                    ArrayList<String> list = new ArrayList<>();
                    list.add(value2);
                    tagsmap.put(key2, list);
                    keys2.add(key2);
                }

                if(!tagscount.containsKey(key2))
                {
                    tagscount.put(key2, (float) 0.0);
                }
                else {
                    Float cc = tagscount.get(key2);
                    cc++;
                    tagscount.remove(key2);
                    tagscount.put(key2, cc);
                }
            }
        }
        Map<String, Integer > Answer = new HashMap<>();
        String writepath = "Matrix.txt";
        FileWriter fw = new FileWriter(writepath);
        BufferedWriter bw = new BufferedWriter(fw);

        for(int i=0;i<keys.size();i++)
        {
            ArrayList<String> list2 = new ArrayList<>(mymap.get(keys.get(i)));
            for(int j=0;j<list2.size();j++){
                for(int k=j+1;k<list2.size();k++)
                {
                    String s1 = list2.get(j);
                    String s2 = list2.get(k);
                    int compare = s1.compareTo(s2);
                    String newKey = null;
                    if (compare < 0){
                        newKey = s1 + "&" + s2;

                    }
                    else{
                        newKey = s2 + "&" + s1;
                    }
                    if(!Answer.containsKey(newKey))
                    {
                        Answer.put(newKey, 0);
                    }
                    else{
                        int count = Answer.get(newKey);
                        count++;
                        Answer.remove(newKey);
                        Answer.put(newKey, count);
                    }
                    newkeys.add(newKey);
                }
            }
        }

        Map<String, Integer> tagTOint = new HashMap<>();
        Map<Integer, String> intTOtag = new HashMap<>();
        int mapping = 1;
        //float Matrix[][] = new float[19000][19000];
        Hashtable<String, Float> results = new Hashtable<>();
        for(int i=0;i<newkeys.size();i++)
        {
            String[] tags = newkeys.get(i).split("&");
            if(!tagTOint.containsKey(tags[0])){
                tagTOint.put(tags[0], mapping);
                mapping++;
            }
            if(!tagTOint.containsKey(tags[1]))
            {
                tagTOint.put(tags[1], mapping);
                mapping++;
            }

            float difference = (float) (1.0 - (float)(Answer.get(newkeys.get(i)))/((float)(tagsmap.get(tags[0]).size() + tagsmap.get(tags[1]).size())));

            results.put(newkeys.get(i), difference);
            //Matrix[tagTOint.get(tags[0])][tagTOint.get(tags[1])] = difference;
            //Matrix[tagTOint.get(tags[1])][tagTOint.get(tags[0])] = difference;
        }


        ArrayList my_arraylist = new ArrayList(tagscount.entrySet());

        Collections.sort(my_arraylist, new MyComparator());


        String[][] Matrix = new String[300][300];

        Iterator itr = my_arraylist.iterator();
        cnt = 0;
        tagTOint.clear();mapping = 1;
        ArrayList<String> porrokhdad = new ArrayList<>();
        while (itr.hasNext()&& cnt < 201)  {
            cnt++;
            Map.Entry e = (Map.Entry) itr.next();
            String tags = (String)e.getKey();
            porrokhdad.add(tags);
        }

        for(int i=0;i<porrokhdad.size();i++)
        {
            for (int j=i+1;j<porrokhdad.size();j++)
            {
                if(!tagTOint.containsKey(porrokhdad.get(i))){
                    tagTOint.put(porrokhdad.get(i), mapping);
                    intTOtag.put(mapping, porrokhdad.get(i));
                    mapping++;
                }
                if(!tagTOint.containsKey(porrokhdad.get(j)))
                {
                    tagTOint.put(porrokhdad.get(j), mapping);
                    intTOtag.put(mapping, porrokhdad.get(j));
                    mapping++;
                }

                float diff = 1;
                int compare = porrokhdad.get(i).compareTo(porrokhdad.get(j));
                String newKey = null;
                if (compare < 0){
                    newKey = porrokhdad.get(i) + "&" + porrokhdad.get(j);

                }
                else{
                    newKey = porrokhdad.get(j) + "&" + porrokhdad.get(i);
                }
                //System.out.println(newKey);
                if(results.containsKey(newKey)){
                    //System.out.println("jhjsdf");
                    diff = results.get(newKey);
                }
                Matrix[0][tagTOint.get(porrokhdad.get(i))] = porrokhdad.get(i);
                Matrix[0][tagTOint.get(porrokhdad.get(j))] = porrokhdad.get(j);
                Matrix[tagTOint.get(porrokhdad.get(j))][0] = porrokhdad.get(j);
                Matrix[tagTOint.get(porrokhdad.get(i))][0] = porrokhdad.get(i);

                Matrix[tagTOint.get(porrokhdad.get(i))][tagTOint.get(porrokhdad.get(j))] = Float.toString(diff);
                Matrix[tagTOint.get(porrokhdad.get(j))][tagTOint.get(porrokhdad.get(i))] = Float.toString(diff);

            }
        }

        //System.out.println(mapping);
        for(int i=0;i<300;i++){
            for(int j=0;j<300;j++){
                if(Matrix[i][j] == null)
                {
                    if(results.containsKey(intTOtag.get(i)+"&"+intTOtag.get(j)))
                    {
                        //System.out.println("hi");
                        Matrix[i][j] = Float.toString(results.get(intTOtag.get(i)+"&"+intTOtag.get(j)));
                    }
                    else if(results.containsKey(intTOtag.get(j)+"&"+intTOtag.get(i))){
                        Matrix[i][j] = Float.toString(results.get(intTOtag.get(j)+"&"+intTOtag.get(i)));
                    }
                    else
                    {
                        Matrix[i][j] = "1";
                    }
                    if(i == j) {
                        Matrix[i][j] = "0";
                    }
                }
            }
        }
        /**/
        for(int i=0;i<mapping;i++)
        {
            for(int j=0;j<mapping;j++)
            {
                //System.out.print(Matrix[i][j] + ',');
                bw.write(Matrix[i][j]);
                bw.write(",");
            }
            //System.out.println();
            bw.write('\n');
        }
        bw.close();

        //System.out.println(results.get("canvas&java"));
    }
}
